package ru.tsc.pavlov.tm.model;

import ru.tsc.pavlov.tm.enumerated.Status;

import java.util.UUID;

public class Project {

    private String id = UUID.randomUUID().toString();

    private String name;

    private Status status = Status.NOT_STARTED;

    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
